{{ $architecture := or .architecture "amd64" }}
{{ $type := or .type "sdk" }}
{{ $mirror := or .mirror "https://repositories.apertis.org/apertis/" }}
{{ $suite := or .suite "17.12" }}
{{ $timestamp := or .timestamp "00000000.0" }}
{{ $ospack := or .ospack (printf "ospack_%s-%s-%s_%s.tar.gz" $suite $architecture $type $timestamp) }}
{{ $image := or .image (printf "apertis-%s-%s-%s_%s" $suite  $type $architecture $timestamp) }}

{{ $cmdline := or .cmdline "console=tty0 console=ttyS0,115200n8 rootwait ro quiet splash plymouth.ignore-serial-consoles" }}

architecture: {{ $architecture }}

actions:

  - action: unpack
    description: Unpack {{ $ospack }}
    compression: gz
    file: {{ $ospack }}

  - action: image-partition
    imagename: {{ $image }}.img
    imagesize: 20G
    partitiontype: gpt

    mountpoints:
      - mountpoint: /
        partition: system
        flags: [ boot ]
        options: ro
      - mountpoint: /boot/efi
        partition: EFI
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: EFI
        fs: vfat
        start: 6176s
        end: 256M
        flags: [ boot ]
      - name: system
        fs: btrfs
        start: 256M
        end: 10G
      - name: general_storage
        fs: btrfs
        start: 10G
        end: 100%

  - action: filesystem-deploy
    description: Deploying ospack onto image

  - action: run
    description: Set NODM
    chroot: true
    command: sed -i -e 's%NODM_ENABLED=.*%NODM_ENABLED=true%' -e 's%NODM_USER=.*%NODM_USER=user%' /etc/default/nodm

  - action: apt
    packages:
      - gummiboot

  - action: run
    description: Install UEFI bootloader
    chroot: true
    command: bootctl --path=/boot/efi install

  - action: run
    description: Add kernel parameters
    chroot: true
    command: sed -i -e 's%GUMMIBOOT_OPTIONS=.*%GUMMIBOOT_OPTIONS="{{ $cmdline }}"%' /etc/default/gummiboot

  - action: apt
    description: Kernel and system packages for amd64
    packages:
      - linux-image-generic
      - btrfs-tools
      - libegl1-mesa-drivers
      - libgles2-mesa

  - action: run
    description: Cleanup /var/lib
    script: scripts/remove_var_lib_parts.binary

  # For VirtualBox
  - action: run
    description: Convert raw image to {{ $image }}.vdi
    postprocess: true
    command: qemu-img convert -O vdi {{ $image }}.img {{ $image }}.vdi

  - action: run
    description: Create block map for {{ $image }}.vdi
    postprocess: true
    command: bmaptool create {{ $image }}.vdi > {{ $image }}.vdi.bmap

  - action: run
    description: Compress {{ $image }}.vdi
    postprocess: true
    command: gzip -f {{ $image }}.vdi

  - action: run
    description: Checksum for {{ $image }}.vdi.gz
    postprocess: true
    command: md5sum {{ $image }}.vdi.gz > {{ $image }}.vdi.gz.md5

  # Raw image
  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create {{ $image }}.img > {{ $image }}.img.bmap

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f {{ $image }}.img

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: md5sum {{ $image }}.img.gz > {{ $image }}.img.gz.md5
